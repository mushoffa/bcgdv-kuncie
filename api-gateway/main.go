package main

import (
	"api-gateway/config"
	// "api-gateway/server"
	"log"

	v1 "api-gateway/controller/v1"
	"api-gateway/router"

	"github.com/kelseyhightower/envconfig"
	client "github.com/mushoffa/go-library/server/grpc"
	server "github.com/mushoffa/go-library/server/http"
	"github.com/mushoffa/bcgdv-kuncie/protos"

	"google.golang.org/grpc"
)

// @Created 15/10/2021
// @Updated 26/10/2021
func main() {
	var cfg config.Config
	err := envconfig.Process("", &cfg)
	if err != nil {
		log.Fatalf("Error loading environment config: %v", err)
	}


	grpcProductClient, err := client.NewGrpcClient(":9091", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Could not connect to Product GRPC Client: %s", err)
	}

	grpcOrderClient, err := client.NewGrpcClient(":9092", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Could not connect to Order GRPC Client: %s", err)
	}

	grpcPromotionClient, err := client.NewGrpcClient(":9093", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Could not connect to Promotion GRPC Client: %s", err)
	}

	grpcProductClientService := protos.NewCatalogServiceClient(grpcProductClient.Conn)
	grpcPromotionClientService := protos.NewPromotionServiceClient(grpcPromotionClient.Conn)
	grpcOrderClientService := protos.NewOrderServiceClient(grpcOrderClient.Conn)
	
	v1Controller := v1.NewV1Controller(grpcProductClientService, grpcPromotionClientService, grpcOrderClientService)

	router := router.NewRouter(v1Controller)
	router.InitializeRouter()

	server := server.NewHttpServer(cfg.ServerPort, router.Gin)
	server.Run()
	// server := server.NewServer(&cfg)
	// server.Run()
}
