package server

import (
	"api-gateway/client"
	"api-gateway/config"
	v1 "api-gateway/controller/v1"
	"api-gateway/domain/entity/pb"
	"api-gateway/domain/usecase"
	"api-gateway/router"
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"google.golang.org/grpc"
)

// @Created 15/10/2021
// @Updated
type Server struct {
	cfg    *config.Config
	router *router.Router
}

// @Created 15/10/2021
// @Updated
func NewServer(cfg *config.Config) *Server {
	return &Server{cfg: cfg}
}

// @Created 15/10/2021
// @Updated
func (s *Server) Run() error {
	// ctx, cancel := context.WithCancel(context.Background())
	// defer cancel()

	// errChannel := make(chan error, 1)
	// signalChannel := make(chan os.Signal, 1)

	// catalogServiceClient, err := client.NewGrpcClient(":9091")

	// orderServiceClient, err := grpc.Dial(":9092", grpc.WithInsecure())
	// if err != nil {
	// 	log.Fatalf("Could not connect to Pomotion Order Client: %s", err)
	// 	errChannel <- err
	// }

	// promotionServiceClient, err := client.NewGrpcClient(":9093")

	// catalogUsecase := usecase.NewCatalogUsecase(pb.NewCatalogServiceClient(catalogServiceClient.Conn))
	// orderUsecase := usecase.NewOrderUsecase(pb.NewOrderServiceClient(orderServiceClient))
	// promotionUsecase := usecase.NewPromotionUsecase(pb.NewPromotionServiceClient(promotionServiceClient.Conn))

	// v1Controller := v1.NewV1Controller(catalogUsecase, orderUsecase, promotionUsecase)
	// s.router = router.NewRouter(v1Controller)
	// s.router.InitializeRouter()

	// server := &http.Server{
	// 	Addr:    s.cfg.ServerPort,
	// 	Handler: s.router.Gin,
	// }

	// go func() {
	// 	log.Println("Server is listening on port: ", s.cfg.ServerPort)
	// 	errChannel <- server.ListenAndServe()
	// }()

	// signal.Notify(signalChannel, syscall.SIGINT, syscall.SIGTERM)

	// select {
	// case err := <-errChannel:
	// 	if err != nil {
	// 		log.Fatalf("Error: %v", err)
	// 	}

	// case quit := <-signalChannel:
	// 	log.Fatalf("signal.Notify: %v", quit)

	// case done := <-ctx.Done():
	// 	log.Fatalf("ctx.Done(): %v", done)
	// }

	// // server.GracefulStop()
	// if err := server.Shutdown(ctx); err != nil {
	// 	log.Fatalf("Server forced to shutdown: %v", err)
	// }

	return nil
}
