package model

// Response ...
// @Created 16/03/2021
// @Updated
type Response struct {
	Data interface{} `json:"data,omitempty"`
	Meta `json:"meta"`
}

// Meta ...
// @Created 16/03/2021
// @Updated
type Meta struct {
	Code    string `json:"code"`
	Message string `json:"message"`
}

// GetDefaultError ...
// @Created 16/03/2021
// @Updated
func (m Meta) GetDefaultError() Meta {
	return Meta{
		Code:    "01",
		Message: m.Message,
		// Status:  false,
	}
}

// GetDefaultSuccess ...
// @Created 16/03/2021
// @Updated
func (m Meta) GetDefaultSuccess() Meta {
	return Meta{
		Code:    "00",
		Message: m.Message,
	}
}
