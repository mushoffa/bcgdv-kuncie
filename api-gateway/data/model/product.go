package model

type CreateProductRequest struct {
	SKU 	string 		`json:"sku" binding:"required"`
	Name 	string 		`json:"name" binding:"required"`
	Price 	float64 	`json:"price" binding:"required"`
	Qty 	uint32 		`json:"qty" binding:"required"`
}

type UpdatePriceRequest struct {
	ID 		string 		`json:"id" binding:"required"`
	SKU 	string 		`json:"sku" binding:"required"`
	Price 	float64 	`json:"price" binding:"required"`
}

type UpdateQuantityRequest struct {
	ID 		string 		`json:"id" binding:"required"`
	SKU 	string 		`json:"sku" binding:"required"`
	Qty 	uint32 		`json:"qty" binding:"required"`
}

type GetProductBySkuRequest struct {

}

type ListProductRequest struct {

}
