package entity

type Cart struct {
	ID   string `json:"id,omitempty"`
	Name string `json:"name"`
}
