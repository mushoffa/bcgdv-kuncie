package entity

type Promotion struct {
	Code string `json:"code"`
	SKU string `json:"sku"`
	Qty uint32 `json:"qty"`
	Freebies string `json:"freebies"`
	FreebiesQty uint32 `json:"freebiesQty"`
	PromotionType string `json:"promotionType"`
}