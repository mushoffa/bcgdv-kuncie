package usecase

// import (
// 	"api-gateway/domain/entity"
// 	"api-gateway/domain/entity/pb"
// 	"context"

// 	"github.com/golang/protobuf/ptypes/empty"
// )

// // @Created 15/10/2021
// // @Updated
// type CatalogUsecase interface {
// 	Create(*entity.Product) (interface{}, error)
// 	GetProductBySKU(string) (interface{}, error)
// 	ListProduct() (interface{}, error)
// }

// // @Created 15/10/2021
// // @Updated 18/10/2021
// type catalogUsecase struct {
// 	client pb.CatalogServiceClient
// }

// // @Created 15/10/2021
// // @Updated 18/10/2021
// func NewCatalogUsecase(client pb.CatalogServiceClient) CatalogUsecase {
// 	return &catalogUsecase{client}
// }

// func (u *catalogUsecase) Create(product *entity.Product) (interface{}, error) {
// 	res, err := u.client.InsertNewProduct(context.Background(), &pb.Product{
// 		Sku:   product.SKU,
// 		Name:  product.Name,
// 		Price: product.Price,
// 		Qty:   product.Qty,
// 	})

// 	if err != nil {
// 		return nil, err
// 	}

// 	return res, nil
// }

// func (u *catalogUsecase) GetProductBySKU(sku string) (interface{}, error) {
// 	res, err := u.client.GetProductBySKU(context.Background(), &pb.GetProductBySKURequest{
// 		Sku: sku,
// 	})

// 	if err != nil {
// 		return nil, err
// 	}

// 	return res, nil
// }

// func (u *catalogUsecase) ListProduct() (interface{}, error) {
// 	res, err := u.client.GetAllProducts(context.Background(), &empty.Empty{})
// 	if err != nil {
// 		return nil, err
// 	}

// 	return res, nil
// }
