package usecase

// import (
// 	"context"

// 	"api-gateway/domain/entity"
// 	"api-gateway/domain/entity/pb"


// 	"github.com/golang/protobuf/ptypes/empty"
// )

// // @Created 15/10/2021
// // @Updated
// type PromotionUsecase interface {
// 	Create(*entity.Promotion) (interface{}, error)
// 	ListPromotion() (interface{}, error)
// }

// // @Created 15/10/2021
// // @Updated
// type promotionUsecase struct {
// 	client pb.PromotionServiceClient
// }

// // @Created 15/10/2021
// // @Updated
// func NewPromotionUsecase(client pb.PromotionServiceClient) PromotionUsecase {
// 	return &promotionUsecase{client}
// }

// func (u *promotionUsecase) Create(promotion *entity.Promotion) (interface{}, error) {
// 	res, err := u.client.CreatePromotion(context.Background(), &pb.CreatePromotionRequest{
// 		Code: promotion.Code,
// 		Sku: promotion.SKU,
// 		Qty: promotion.Qty,
// 		Freebies: promotion.Freebies,
// 		FreebiesQty: promotion.FreebiesQty,
// 		Type: promotion.PromotionType,
// 	})

// 	if err != nil {
// 		return nil, err
// 	}

// 	return res, nil
// }

// func (u *promotionUsecase) ListPromotion() (interface{}, error) {
// 	res, err := u.client.GetAllPromotions(context.Background(), &empty.Empty{})
// 	if err != nil {
// 		return nil, err
// 	}

// 	return res, nil
// }
