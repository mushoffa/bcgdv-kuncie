package v1

import (
	"github.com/mushoffa/bcgdv-kuncie/protos"
)

// @Created 15/10/2021
// @Updated 26/10/2021
type V1Controller struct {
	Product   ProductController
	Order     OrderController
	Promotion PromotionController
}

// @Created 15/10/2021
// @Updated 26/10/2021
func NewV1Controller(product protos.CatalogServiceClient, promotion protos.PromotionServiceClient, order protos.OrderServiceClient) *V1Controller {
	return &V1Controller{
		Product:   NewProductController(product),
		Order:     NewOrderController(order),
		Promotion: NewPromotionController(promotion),
	}
}
