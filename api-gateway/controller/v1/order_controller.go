package v1

import (
	"context"
	"net/http"

	"api-gateway/data/model"
// 	"api-gateway/domain/usecase"

	"github.com/gin-gonic/gin"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/mushoffa/bcgdv-kuncie/protos"
)

// @Created 15/10/2021
// @Updated 20/10/2021
type OrderController interface {
	// Create(*gin.Context)
	CreateCart(*gin.Context)
	GetCartByID(*gin.Context)
	ListCart(*gin.Context)
	AddItem(*gin.Context)
	RemoveItem(*gin.Context)
	Checkout(*gin.Context)
	
}

// @Created 15/10/2021
// @Updated 26/10/2021
type orderController struct {
	client protos.OrderServiceClient
}

// @Created 15/10/2021
// @Updated 26/10/2021
func NewOrderController(client protos.OrderServiceClient) OrderController {
	return &orderController{client}
}

// @Created 15/10/2021
// @Updated 26/10/2021
// func (c *orderController) Create(ctx *gin.Context) {
// 	parent := context.Background()
// 	defer parent.Done()

// 	meta := model.Meta{}
// 	request := model.CreateCartRequest{}
// 	response := model.Response{Meta: meta.GetDefaultError()}
// }

// @Created 15/10/2021
// @Updated 26/10/2021
func (c *orderController) CreateCart(ctx *gin.Context) {
	parent := context.Background()
	defer parent.Done()

	meta := model.Meta{}
	request := model.CreateCartRequest{}
	response := model.Response{Meta: meta.GetDefaultError()}

	if err := ctx.ShouldBindJSON(&request); err != nil {
		response.Meta.Message = err.Error()
		ctx.JSON(http.StatusBadRequest, err.Error())
		return
	}

	res, err := c.client.CreateCart(context.Background(), &protos.CreateCartRequest{
		Name: request.Name,
	})
	if err != nil {
		response.Meta.Message = err.Error()
		ctx.JSON(http.StatusBadRequest, response)
		return
	}

	response.Data = res
	meta.Message = "Success"
	response.Meta = meta.GetDefaultSuccess()
	ctx.JSON(http.StatusOK, response)
}

// @Created 20/10/2021
// @Updated 26/10/2021
func (c *orderController) GetCartByID(ctx *gin.Context) {
	parent := context.Background()
	defer parent.Done()

	meta := model.Meta{}
	response := model.Response{Meta: meta.GetDefaultError()}

	cartID := ctx.Param("cartId")

	res, err := c.client.GetCartByID(context.Background(), &protos.GetCartByIDRequest{
		Id: cartID,
	})
	if err != nil {
		response.Meta.Message = err.Error()
		ctx.JSON(http.StatusBadRequest, response)
		return
	}

	response.Data = res
	meta.Message = "Success"
	response.Meta = meta.GetDefaultSuccess()
	ctx.JSON(http.StatusOK, response)
}

// @Created 20/10/2021
// @Updated
func (c *orderController) ListCart(ctx *gin.Context) {
	parent := context.Background()
	defer parent.Done()

	meta := model.Meta{}
	response := model.Response{Meta: meta.GetDefaultError()}

	res, err := c.client.GetAllCarts(context.Background(), &empty.Empty{})
	if err != nil {
		response.Meta.Message = err.Error()
		ctx.JSON(http.StatusBadRequest, response)
		return
	}

	response.Data = res.Carts
	meta.Message = "Success"
	response.Meta = meta.GetDefaultSuccess()
	ctx.JSON(http.StatusOK, response)
}

// @Created 20/10/2021
// @Updated
func (c *orderController) AddItem(ctx *gin.Context) {
	parent := context.Background()
	defer parent.Done()
}

// @Created 20/10/2021
// @Updated
func (c *orderController) RemoveItem(ctx *gin.Context) {
	parent := context.Background()
	defer parent.Done()
}

// @Created 20/10/2021
// @Updated
func (c *orderController) Checkout(ctx *gin.Context) {
	parent := context.Background()
	defer parent.Done()
}