package v1

import (
	"context"
	"net/http"

	"api-gateway/data/model"
// 	"api-gateway/domain/entity"
// 	"api-gateway/domain/usecase"

	"github.com/gin-gonic/gin"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/mushoffa/bcgdv-kuncie/protos"
)

// @Created 15/10/2021
// @Updated 26/102021
type PromotionController interface {
	Create(*gin.Context)
	// GetPromotionByID(*gin.Context)
	ListPromotion(*gin.Context)
	Update(*gin.Context)
}

// @Created 15/10/2021
// @Updated 26/10/2021
type promotionController struct {
	client protos.PromotionServiceClient
}

// @Created 15/10/2021
// @Updated
func NewPromotionController(client protos.PromotionServiceClient) PromotionController {
	return &promotionController{client}
}

// @Created 15/10/2021
// @Updated
func (c *promotionController) Create(ctx *gin.Context) {
	parent := context.Background()
	defer parent.Done()

	meta := model.Meta{}
	request := model.CreatePromotionRequest{}
	response := model.Response{Meta: meta.GetDefaultError()}

	if err := ctx.ShouldBindJSON(&request); err != nil {
		response.Meta.Message = err.Error()
		ctx.JSON(http.StatusBadRequest, response)
		return
	}

	res, err := c.client.CreatePromotion(context.Background(), &protos.CreatePromotionRequest{
		Code: request.Code,
	})
	if err != nil {
		response.Meta.Message = err.Error()
		ctx.JSON(http.StatusBadRequest, response)
		return
	}

	response.Data = res.Promotion
	meta.Message = "Success"
	response.Meta = meta.GetDefaultSuccess()
	ctx.JSON(http.StatusOK, response)
}

// @Created 26/10/2021
// @Updated
// func (c *promotionController) GetPromotionByID(ctx *gin.Context) {
// 	parent := context.Background()
// 	defer parent.Done()

// 	meta := model.Meta{}
// 	response := model.Response{Meta: meta.GetDefaultError()}

// 	promotionID := ctx.Param("id")

// 	res, err := c.client.GetPromotionByID(context.Background(), &protos.GetPromotionByID)
// }

func (c *promotionController) ListPromotion(ctx *gin.Context) {
	parent := context.Background()
	defer parent.Done() 

	meta := model.Meta{}
	response := model.Response{Meta: meta.GetDefaultError()}

	res, err := c.client.GetAllPromotions(context.Background(), &empty.Empty{})
	if err != nil {
		response.Meta.Message = err.Error()
		ctx.JSON(http.StatusBadRequest, response)
		return
	}

	response.Data = res.Promotions
	meta.Message = "Success"
	response.Meta = meta.GetDefaultSuccess()
	ctx.JSON(http.StatusOK, response)
}

// @Created 15/10/2021
// @Updated
func (c *promotionController) Update(ctx *gin.Context) {
	parent := context.Background()
	defer parent.Done() 

	meta := model.Meta{}
	response := model.Response{Meta: meta.GetDefaultError()}

	// res, err := c.u.ListPromotion()
	// if err != nil {
	// 	response.Meta.Message = err.Error()
	// 	ctx.JSON(http.StatusBadRequest, response)
	// 	return
	// }

	// response.Data = res
	// meta.Message = "Approved"
	response.Meta = meta.GetDefaultSuccess()
	ctx.JSON(http.StatusOK, response)
}