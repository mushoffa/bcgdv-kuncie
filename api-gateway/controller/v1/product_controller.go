package v1

import (
	"context"
	"net/http"

	"api-gateway/data/model"

	"github.com/gin-gonic/gin"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/mushoffa/bcgdv-kuncie/protos"
)

// @Created 15/10/2021
// @Updated 26/10/2021
type ProductController interface {
	Create(*gin.Context)
	UpdatePrice(*gin.Context)
	UpdateQuantity(*gin.Context)
	GetProductBySKU(*gin.Context)
	ListProduct(*gin.Context)
}

// @Created 15/10/2021
// @Updated 26/10/2021
type productController struct {
	client protos.CatalogServiceClient
}

// @Created 15/10/2021
// @Updated 26/10/2021
func NewProductController(client protos.CatalogServiceClient) ProductController {
	return &productController{client}
}

// @Created 15/10/2021
// @Updated 26/10/2021
func (c *productController) Create(ctx *gin.Context) {
	parent := context.Background()
	defer parent.Done()

	meta := model.Meta{}
	request := model.CreateProductRequest{}
	response := model.Response{Meta: meta.GetDefaultError()}

	if err := ctx.ShouldBindJSON(&request); err != nil {
		response.Meta.Message = err.Error()
		ctx.JSON(http.StatusBadRequest, response)
		return
	}

	res, err := c.client.CreateProduct(context.Background(), &protos.CreateProductRequest{
		Sku: request.SKU,
		Name: request.Name,
		Price: request.Price,
		Qty: request.Qty,
	})
	if err != nil {
		response.Meta.Message = err.Error()
		ctx.JSON(http.StatusBadRequest, response)
		return
	}

	response.Data = res.Product
	meta.Message = "Success"
	response.Meta = meta.GetDefaultSuccess()
	ctx.JSON(http.StatusOK, response)
}

// @Created 15/10/2021
// @Updated 26/10/2021
func (c *productController) UpdatePrice(ctx *gin.Context) {
	parent := context.Background()
	defer parent.Done()

	meta := model.Meta{}
	request := model.UpdatePriceRequest{}
	response := model.Response{Meta: meta.GetDefaultError()}

	if err := ctx.ShouldBindJSON(&request); err != nil {
		response.Meta.Message = err.Error()
		ctx.JSON(http.StatusBadRequest, response)
		return
	}

	res, err := c.client.UpdatePrice(context.Background(), &protos.UpdatePriceRequest{
		Id: request.ID,
		Sku: request.SKU,
		Price: request.Price,
	})
	if err != nil {
		response.Meta.Message = err.Error()
		ctx.JSON(http.StatusBadRequest, response)
		return
	}

	response.Data = res.Product
	meta.Message = "Success"
	response.Meta = meta.GetDefaultSuccess()
	ctx.JSON(http.StatusOK, response)
}

// @Created 26/10/2021
// @Updated 
func (c *productController) UpdateQuantity(ctx *gin.Context) {
	parent := context.Background()
	defer parent.Done()

	meta := model.Meta{}
	request := model.UpdateQuantityRequest{}
	response := model.Response{Meta: meta.GetDefaultError()}

	if err := ctx.ShouldBindJSON(&request); err != nil {
		response.Meta.Message = err.Error()
		ctx.JSON(http.StatusBadRequest, response)
		return
	}

	res, err := c.client.UpdateQuantity(context.Background(), &protos.UpdateQuantityRequest{
		Id: request.ID,
		Sku: request.SKU,
		Qty: request.Qty,
	})
	if err != nil {
		response.Meta.Message = err.Error()
		ctx.JSON(http.StatusBadRequest, response)
		return
	}

	response.Data = res.Product
	meta.Message = "Success"
	response.Meta = meta.GetDefaultSuccess()
	ctx.JSON(http.StatusOK, response)
}

// @Created 18/10/2021
// @Updated
func (c *productController) GetProductBySKU(ctx *gin.Context) {
	parent := context.Background()
	defer parent.Done()

	meta := model.Meta{}
	// request := model.CreateProductRequest{}
	response := model.Response{Meta: meta.GetDefaultError()}

	sku := ctx.Param("sku")

	res, err := c.client.GetProductBySKU(context.Background(), &protos.GetProductBySKURequest{
		Sku: sku,
	})
	if err != nil {
		response.Meta.Message = err.Error()
		ctx.JSON(http.StatusBadRequest, response)
		return
	}

	response.Data = res.Product
	meta.Message = "Success"
	response.Meta = meta.GetDefaultSuccess()
	ctx.JSON(http.StatusOK, response)
}

// @Created 18/10/2021
// @Updated
func (c *productController) ListProduct(ctx *gin.Context) {
	parent := context.Background()
	defer parent.Done()

	meta := model.Meta{}
	response := model.Response{Meta: meta.GetDefaultError()}

	res, err := c.client.GetAllProducts(context.Background(), &empty.Empty{})
	if err != nil {
		response.Meta.Message = err.Error()
		ctx.JSON(http.StatusBadRequest, response)
		return
	}

	response.Data = res.Products
	meta.Message = "Success"
	response.Meta = meta.GetDefaultSuccess()
	ctx.JSON(http.StatusOK, response)
}
