package client

import "google.golang.org/grpc"

type GrpcClient struct {
	Conn *grpc.ClientConn
}

func NewGrpcClient(url string) (*GrpcClient, error) {
	conn, err := grpc.Dial(url, grpc.WithInsecure())
	if err != nil {
		return nil, err
	}

	// defer conn.Close()
	return &GrpcClient{conn}, nil
}
