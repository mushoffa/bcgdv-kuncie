package router

import (
	v1 "api-gateway/controller/v1"

	"github.com/gin-gonic/gin"
)

// @Created 15/10/2021
// @Updated
type Router struct {
	Gin *gin.Engine
	v1  *v1.V1Controller
}

// @Created 15/10/2021
// @Updated
func NewRouter(v1 *v1.V1Controller) *Router {
	return &Router{
		Gin: gin.Default(),
		v1:  v1,
	}
}

// @Created 15/10/2021
// @Updated
func (r *Router) InitializeRouter() {
	r.InitializeV1Router()
}

// @Created 15/10/2021
// @Updated 26/10/2021
func (r *Router) InitializeV1Router() {
	api := r.Gin.Group("/api")
	v1 := api.Group("/v1")

	product := v1.Group("/product")
	product.POST("/create", r.v1.Product.Create)
	product.POST("/update/price", r.v1.Product.UpdatePrice)
	product.POST("/update/quantity", r.v1.Product.UpdateQuantity)
	product.GET("/:sku", r.v1.Product.GetProductBySKU)
	product.GET("/list", r.v1.Product.ListProduct)

	order := v1.Group("/order")
	// // order.POST("/", r.v1.Order.Create)
	order.POST("/cart/create", r.v1.Order.CreateCart)
	order.GET("/cart/:cartId", r.v1.Order.GetCartByID)
	order.GET("/cart/list", r.v1.Order.ListCart)
	order.POST("/addItem", r.v1.Order.AddItem)
	order.POST("/removeItem", r.v1.Order.RemoveItem)
	order.POST("/checkout", r.v1.Order.Checkout)

	promotion := v1.Group("/promotion")
	promotion.POST("/create", r.v1.Promotion.Create)
	promotion.POST("/update", r.v1.Promotion.Update)
	// promotion.GET("/:id")
	promotion.GET("/list", r.v1.Promotion.ListPromotion)

}
