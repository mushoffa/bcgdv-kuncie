package config

// @Created 15/10/2021
// @Updated
type Config struct {
	ServerPort int `envconfig:"SERVER_PORT"`
}
