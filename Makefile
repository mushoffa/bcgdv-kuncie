deploy:
	docker-compose up -d --build
	

protos:
	#protoc catalog-service/catalog.proto --go_out=plugins=grpc:catalog-service/domain/entity/pb
	protoc --go_out=./catalog-service/domain/entity/pb/ --go_opt=paths=source_relative --go-grpc_out=./catalog-service/domain/entity/pb/ --go-grpc_opt=paths=source_relative ./catalog-service/catalog.proto ;\