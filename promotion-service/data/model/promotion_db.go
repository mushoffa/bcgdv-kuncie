package model

import "time"

// @Created 13/10/2021
// @Updated
type Promotion struct {
	ID       string    `gorm:"column:id"`
	Created  time.Time `gorm:"column:created"`
	Updated  time.Time `gorm:"column:updated"`
	Code     string    `gorm:"column:code"`
	SKU      string    `gorm:"column:sku"`
	Qty      uint32    `gorm:"column:qty"`
	Freebies string    `gorm:"column:freebies"`
	FreebiesQty uint32 `gorm:"column:freebies_qty"`
	Type     string    `gorm:"column:type"`
}

// @Created 13/10/2021
// @Updated
func (t *Promotion) TableName() string {
	return "promotions"
}
