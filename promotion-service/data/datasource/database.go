package datasource

import (
	"promotion-service/data/model"

	"github.com/jinzhu/gorm"
	"github.com/mushoffa/go-library/database"
)

// @Created 12/10/2021
// @Updated 18/10/2021
type Database interface {
	database.Database
	InitializeDatabase() error
	UpdateByModel(*model.Promotion) error
	// Reader
	// Writer
}

// @Created 12/10/2021
// @Updated 14/10/2021
type Reader interface {
	GetInstance() *gorm.DB
	FindByID(string, string, interface{}) error
	FindAll(interface{}) (interface{}, error)
}

// @Created 12/10/2021
// @Updated
type Writer interface {
	//AutoMigrate(...interface{})
	Create(interface{}) error
	//UpdateByID(string, string, string, string, interface{}) (error)
}
