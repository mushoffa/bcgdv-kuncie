package postgres

import (
	"promotion-service/config"
	"promotion-service/data/datasource"
	"promotion-service/data/model"

	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/mushoffa/go-library/database"
)

const (
	maxOpenConns    = 60
	connMaxLifetime = 120
	maxIdleConns    = 30
	connMaxIdleTime = 20
)

// @Created 14/10/2021
// @Updated 18/10/2021
type PromotionDB struct {
	// Gorm *gorm.DB
	database.Database
}

// @Created 14/10/2021
// @Updated
func NewPromotionDB(cfg *config.Config) (datasource.Database, error) {
	db, err := database.NewPostgres(
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresDB,
		cfg.PostgresUser,
		cfg.PostgresPassword,
		false,
	)

	promotionDB := &PromotionDB{db}
	return promotionDB, err
}

func (db *PromotionDB) InitializeDatabase() error {
	db.AutoMigrate(&model.Promotion{})
	return nil
}

// @Created 18/10/2021
// @Updated
func (db *PromotionDB) UpdateByModel(model *model.Promotion) error {
	if err := db.GetInstance().Model(&model).Updates(&model).Error; err != nil {
		return err
	}
	return nil
}

// // @Created 14/10/2021
// // @Updated
// func (db *PromotionDB) GetInstance() *gorm.DB {
// 	return db.Gorm
// }

// // @Created 14/10/2021
// // @Updated
// func (db *PromotionDB) FindByID(queryField, queryID string, data interface{}) error {
// 	query := fmt.Sprintf("%s = ?", queryField)

// 	if err := db.Gorm.Model(data).Where(query, queryID).Take(data).Error; err != nil {
// 		return err
// 	}

// 	return nil
// }

// // @Created 14/10/2021
// // @Updated
// func (db *PromotionDB) FindAll(data interface{}) (interface{}, error) {

// 	if err := db.Gorm.Find(data).Error; err != nil {
// 		return nil, err
// 	}
// 	return nil, nil
// }

// // @Created 14/10/2021
// // @Updated
// func (db *PromotionDB) Create(model interface{}) error {
// 	if err := db.Gorm.Create(model).Error; err != nil {
// 		return err
// 	}

// 	return nil
// }
