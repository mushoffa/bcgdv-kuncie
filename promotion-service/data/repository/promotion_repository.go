package repository

import (
	"log"
	"time"

	"promotion-service/data/datasource"
	"promotion-service/data/model"
	"promotion-service/domain/entity"
	domain "promotion-service/domain/repository"

	"github.com/google/uuid"
)

// @Created 14/10/2021
// @Updated
type promotionRepository struct {
	db datasource.Database
}

// @Created 14/10/2021
// @Updated
func NewPromotionRepository(db datasource.Database) domain.PromotionRepository {
	return &promotionRepository{db}
}

func (r *promotionRepository) Initialize() error {
	r.db.InitializeDatabase()
	return nil
}

// @Created 14/10/2021
// @Updated
func (r *promotionRepository) InitializePromotions() error {
	promotions := []model.Promotion{
		{
			ID:       uuid.NewString(),
			Created:  time.Now(),
			Code:     "BXYGZ",
			SKU:      "43N23P",
			Qty:      1,
			Freebies: "234234",
			Type:     "ITEM",
		},
		{
			ID:       uuid.NewString(),
			Created:  time.Now(),
			Code:     "BXYPZ",
			SKU:      "120P90",
			Qty:      3,
			Freebies: "2",
			Type:     "PRICE",
		},
		{
			ID:       uuid.NewString(),
			Created:  time.Now(),
			Code:     "BXYDZ",
			SKU:      "A304SD",
			Qty:      3,
			Freebies: "0.1", // 10% discount
			Type:     "DISCOUNT",
		},
	}

	for _, promotion := range promotions {
		r.db.Create(&promotion)
	}

	log.Println(promotions)

	// if err := r.db.Create(&promotions); err != nil {
	// 	return err
	// }

	return nil
}

// @Created 14/10/2021
// @Updated 18/10/2021
func (r *promotionRepository) Insert(promotion *entity.Promotion) error {
	id := uuid.NewString()
	created := time.Now()

	promotionDB := model.Promotion{
		ID:          id,
		Created:     created,
		Code:        promotion.Code,
		SKU:         promotion.SKU,
		Qty:         promotion.Qty,
		Freebies:    promotion.Freebies,
		FreebiesQty: promotion.FreebiesQty,
		Type:        promotion.Type,
	}

	if err := r.db.Create(&promotionDB); err != nil {
		return err
	}

	promotion.ID = id
	promotion.Created = created

	return nil
}

// @Created 18/10/2021
// @Updated
func (r *promotionRepository) GetAll() ([]entity.Promotion, error) {
	promotions := []entity.Promotion{}

	if err := r.db.FindAll(&promotions); err != nil {
		return nil, err
	}

	return promotions, nil
}

// @Created 14/10/2021
// @Updated 18/10/2021
func (r *promotionRepository) GetBySKU(sku string) (*entity.Promotion, error) {
	promotion := entity.Promotion{}

	if err := r.db.FindByID("sku", sku, &promotion); err != nil {
		return nil, err
	}

	return &promotion, nil
}

// @Created 18/10/2021
// @Updated
func (r *promotionRepository) UpdatePromotion(promotion *entity.Promotion) error {

	updated := time.Now()

	err := r.db.GetInstance().Model(&model.Promotion{}).
		Where("id = ?", promotion.ID).
		Where("code = ? ", promotion.Code).
		Where("sku = ?", promotion.SKU).
		Update("qty", promotion.Qty).
		Update("freebies", promotion.Freebies).
		Update("freebies_qty", promotion.FreebiesQty).
		Update("type", promotion.Type).
		Update("updated", updated).Error
	if err != nil {
		return err
	}

	return nil
}
