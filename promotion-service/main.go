package main

import (
	"log"
	
	"promotion-service/config"
	"promotion-service/server"

	"github.com/kelseyhightower/envconfig"
)

// @Created 14/10/2021
// @Updated 
func main() {
	var cfg config.Config
	err := envconfig.Process("", &cfg)
	if err != nil {
		log.Fatalf("Error loading environment config: %v", err)
	}

	server := server.NewServer(&cfg)
	server.Run()
}