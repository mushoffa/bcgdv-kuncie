module promotion-service

go 1.15

require (
	github.com/golang/protobuf v1.4.3
	github.com/google/uuid v1.3.0
	github.com/jinzhu/gorm v1.9.16
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/mushoffa/go-library v0.0.0-20211016054407-67a21adc8585
	google.golang.org/grpc v1.41.0
	google.golang.org/protobuf v1.25.0
)
