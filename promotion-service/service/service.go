package service

import (
	"context"
	_ "log"

	"promotion-service/domain/entity"
	"promotion-service/domain/entity/pb"
	"promotion-service/domain/usecase"

	"github.com/golang/protobuf/ptypes/empty"
)

// @Created 13/10/2021
// @Updated
type promotionService struct {
	pb.UnimplementedPromotionServiceServer
	u usecase.PromotionUsecase
}

// @Created 13/10/2021
// @Updated
func NewPromotionService(u usecase.PromotionUsecase) pb.PromotionServiceServer {
	return &promotionService{u: u}
}

// @Created 13/10/2021
// @Updated 18/10/2021
func (s *promotionService) CreatePromotion(ctx context.Context, request *pb.CreatePromotionRequest) (*pb.CreatePromotionResponse, error) {
	response := pb.CreatePromotionResponse{}

	promotion := entity.Promotion{
		Code:        request.Code,
		SKU:         request.Sku,
		Qty:         request.Qty,
		Freebies:    request.Freebies,
		FreebiesQty: request.FreebiesQty,
		Type:        request.Type,
	}

	err := s.u.CreateNewPromotion(&promotion)
	if err != nil {
		return &response, err
	}

	response.Promotion = &pb.Promotion{
		Id: promotion.ID,
		// Created:     promotion.Created.MarshalBinary(),
		Code:        promotion.Code,
		Sku:         promotion.SKU,
		Qty:         promotion.Qty,
		Freebies:    promotion.Freebies,
		FreebiesQty: promotion.FreebiesQty,
		Type:        promotion.Type,
	}

	response.Promotion.Created, _ = promotion.Created.MarshalBinary()
	return &response, nil
}

// @Created 13/10/2021
// @Updated 18/10/2021
func (s *promotionService) GetAllPromotions(ctx context.Context, request *empty.Empty) (*pb.GetAllPromotionsResponse, error) {
	_promotions, err := s.u.GetAllPromotions()
	if err != nil {
		return nil, err
	}

	promotions := []*pb.Promotion{}
	for _, _promotion := range _promotions {
		promotion := &pb.Promotion{
			Id:          _promotion.ID,
			Code:        _promotion.Code,
			Sku:         _promotion.SKU,
			Qty:         _promotion.Qty,
			Freebies:    _promotion.Freebies,
			FreebiesQty: _promotion.FreebiesQty,
			Type:        _promotion.Type,
		}

		promotions = append(promotions, promotion)
	}

	response := pb.GetAllPromotionsResponse{Promotions: promotions}

	return &response, nil
}

// @Created 13/10/2021
// @Updated 18/10/2021
func (s *promotionService) UpdatePromotion(ctx context.Context, request *pb.UpdatePromotionRequest) (*pb.UpdatePromotionResponse, error) {
	return nil, nil
}

// @Created 13/10/2021
// @Updated
func (s *promotionService) DeletePromotion(ctx context.Context, request *pb.DeletePromotionRequest) (*pb.DeletePromotionResponse, error) {
	return nil, nil
}

// @Created 13/10/2021
// @Updated
func (s *promotionService) CheckPromotion(ctx context.Context, request *pb.CheckPromotionRequest) (*pb.CheckPromotionResponse, error) {
	promoType, promoItem, promoQty, err := s.u.ValidatePromotion(request.Sku, request.Qty)
	if err != nil {
		return nil, err
	}

	return &pb.CheckPromotionResponse{
		PromotionType: promoType,
		Freebies:      promoItem,
		Qty:           promoQty,
	}, nil
}
