package domain

import (
	"promotion-service/domain/entity"
)

// @Created 14/10/2021
// @Updated 18/10/2021
type PromotionRepository interface {
	Initialize() error
	InitializePromotions() error
	Insert(*entity.Promotion) error
	GetAll() ([]entity.Promotion, error)
	// CreatePromotion(*entity.Promotion) (string, error)
	// UpdatePromotion()
	// DeletePromotion()
	GetBySKU(string) (*entity.Promotion, error)
	UpdatePromotion(*entity.Promotion) error
}
