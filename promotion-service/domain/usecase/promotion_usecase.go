package usecase

import (
	"errors"

	"promotion-service/domain/entity"
	domain "promotion-service/domain/repository"

	"github.com/jinzhu/gorm"
)

type PromoFn func(string, uint32)

// var promotionRules = map[string]PromoFn{
// "BYXGZ": BuyXYGetZ,
// "BYXPZ": BuyXYPayZ,
// }

// @Created 13/10/2021
// @Updated 18/10/2021
type PromotionUsecase interface {
	CreateNewPromotion(*entity.Promotion) error
	GetAllPromotions() ([]entity.Promotion, error)
	UpdatePromotion(*entity.Promotion) error
	CheckPromotion(string) (bool, *entity.Promotion, error)
	CheckExistingPromotion(string, string, uint32) error
	ValidatePromotion(string, uint32) (string, string, uint32, error)
	BuyXYGetZ(string, uint32, *entity.Promotion) (string, string, uint32, error)
	BuyXYPayZ(string, uint32, *entity.Promotion) (string, string, uint32, error)
	BuyXYDiscountZ(string, uint32, *entity.Promotion) (string, string, uint32, error)
}

// @Created 13/10/2021
// @Updated
type promotionUsecase struct {
	r domain.PromotionRepository
}

// @Created 13/10/2021
// @Updated
func NewPromotionUsecase(r domain.PromotionRepository) PromotionUsecase {
	// r.InitializePromotions()
	return &promotionUsecase{r}
}

// @Created 14/10/2021
// @Updated 18/10/2021
func (u *promotionUsecase) CreateNewPromotion(promo *entity.Promotion) error {
	// if err := u.CheckExistingPromotion(promo.Code, promo.SKU, promo.Qty); err != nil {
	// 	return err
	// }

	if err := u.r.Insert(promo); err != nil {
		return err
	}

	return nil
}

// @Created 18/10/2021
// @Updated
func (u *promotionUsecase) UpdatePromotion(promotion *entity.Promotion) error {

	return nil
}

// @Created 14/10/20218
// @Updated
func (u *promotionUsecase) GetAllPromotions() ([]entity.Promotion, error) {
	return u.r.GetAll()
}

// @Created 18/10/2021
func (u *promotionUsecase) CheckPromotion(sku string) (bool, *entity.Promotion, error) {
	promotion, err := u.r.GetBySKU(sku)
	if err != nil {
		// Success query, however no record found
		// Alter error to nil, to prevent false positive condition
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return false, nil, nil
		}

		return false, nil, err
	}

	return true, promotion, nil
}

// @Created 14/10/2021
// @Updated 18/10/2021
func (u *promotionUsecase) CheckExistingPromotion(promotionCode, sku string, qty uint32) error {
	isExist, _promotion, err := u.CheckPromotion(sku)
	if err != nil {
		return err
	}

	if !isExist {
		return errors.New("Promotion not found")
	}

	if (promotionCode == _promotion.Code) &&
		(sku == _promotion.SKU) &&
		(qty == _promotion.Qty) {
		return errors.New("Promotion with designated conditions is already exist")
	}
	return nil
}

// @Created 14/10/2021
// @Updated 18/10/2021
func (u *promotionUsecase) ValidatePromotion(sku string, qty uint32) (string, string, uint32, error) {
	res, err := u.r.GetBySKU(sku)
	if err != nil {
		return "", "", 0, err
	}

	switch res.Code {
	case "BXYGZ":
		return u.BuyXYGetZ(sku, qty, res)
	case "BXYPZ":
		return u.BuyXYPayZ(sku, qty, res)
	case "BXYDZ":
		return u.BuyXYDiscountZ(sku, qty, res)
	}

	return "", "", 0, errors.New("Invalid promotion code")
}

// @Created 13/10/2021
// @Updated 14/10/2021
func (u *promotionUsecase) BuyXYGetZ(sku string, qty uint32, promo *entity.Promotion) (string, string, uint32, error) {
	if qty < promo.Qty {
		return "", "", 0, errors.New("Order item does not fulfill minimum quantity")
	}

	// freebiesQty := uint32()

	return promo.Type, promo.Freebies, promo.FreebiesQty, nil
}

// @Created 13/10/2021
// @Updated 14/10/2021
func (u *promotionUsecase) BuyXYPayZ(sku string, qty uint32, promo *entity.Promotion) (string, string, uint32, error) {
	// truthTable := map[uint32]uint32{}

	if qty < promo.Qty {
		return "", "", 0, errors.New("Order item does not fulfill minimum quantity")
	}

	return promo.Type, promo.Freebies, promo.FreebiesQty, nil
}

// @Created 13/10/2021
// @Updated 14/10/2021
func (u *promotionUsecase) BuyXYDiscountZ(sku string, qty uint32, promo *entity.Promotion) (string, string, uint32, error) {
	return promo.Type, promo.Freebies, promo.FreebiesQty, nil
}
