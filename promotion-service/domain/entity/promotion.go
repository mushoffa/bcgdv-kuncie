package entity

import "time"

// @Created 14/10/2021
// @Updated 18/10/2021
type Promotion struct {
	ID          string    `json:"id"`
	Created     time.Time `json:"created"`
	Code        string    `json:"code"`
	SKU         string    `json:"sku"`
	Qty         uint32    `json:"qty"`
	Freebies    string    `json:"freebies"`
	FreebiesQty uint32    `json:"freebies_qty"`
	Type        string    `json:"type"`
}
