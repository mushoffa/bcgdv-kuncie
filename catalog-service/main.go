package main

import (
	"log"
	_ "net"

	"catalog-service/config"
	"catalog-service/server"

	"github.com/kelseyhightower/envconfig"
	_ "google.golang.org/grpc"
)

// @Created 12/10/2021
// @Updated
func main() {
	var cfg config.Config
	err := envconfig.Process("", &cfg)
	if err != nil {
		log.Fatalf("Error loading environment config: %v", err)
	}

	server := server.NewServer(&cfg)
	server.Run()
}