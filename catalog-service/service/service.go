package service

import (
	"context"
	// "log"

	"catalog-service/domain/entity"
	// "catalog-service/domain/entity/pb"
	_ "catalog-service/domain/service"
	"catalog-service/domain/usecase"

	"github.com/golang/protobuf/ptypes/empty"
	"github.com/mushoffa/bcgdv-kuncie/protos"
)

// @Created 12/10/2021
// @Updated 26/10/2021
type catalogService struct {
	protos.UnimplementedCatalogServiceServer
	u usecase.ProductUsecase
}

// @Created 12/10/2021
// @Updated 26/10/2021
func NewCatalogService(u usecase.ProductUsecase) protos.CatalogServiceServer {
	return &catalogService{u: u}
}

// @Created 12/10/2021
// @Updated 17/10/2021
func (s *catalogService) CreateProduct(ctx context.Context, request *protos.CreateProductRequest) (*protos.CreateProductResponse, error) {
	product := entity.Product{
		SKU:   request.Sku,
		Name:  request.Name,
		Price: request.Price,
		Qty:   request.Qty,
	}

	response := protos.CreateProductResponse{}

	res, err := s.u.InsertNewProduct(&product)
	if err != nil {
		return &response, err
	}

	// response.Id = res.ID
	// response.CreatedAt, _ = res.Created.MarshalBinary()
	// response.Sku = res.SKU
	// response.Price = res.Price
	// response.Qty = res.Qty

	response.Product = &protos.Product{
		Id: res.ID,
		Created: res.Created.String(),
		Sku: res.SKU,
		Price: res.Price,
		Qty: res.Qty,
	}

	return &response, nil
}

// @Created 12/10/2021
// @Updated 13/10/2021
func (s *catalogService) GetProductBySKU(ctx context.Context, request *protos.GetProductBySKURequest) (*protos.GetProductResponse, error) {
	response := protos.GetProductResponse{}

	product, err := s.u.GetProductBySKU(request.Sku)
	if err != nil {
		return &response, err
	}

	response.Product = &protos.Product{
		Id:    product.ID,
		Name:  product.Name,
		Price: product.Price,
		Qty:   product.Qty,
	}

	return &response, nil
}

// @Created 12/10/2021
// @Updated
// func (s *catalogService) GetProductByName(ctx context.Context, request *pb.GetProductByNameRequest) (*pb.GetProductResponse, error) {
// 	log.Println("GetProductByName")
// 	return nil, nil
// }

// @Created 12/10/2021
// @Updated 17/10/2021
func (s *catalogService) GetAllProducts(ctx context.Context, request *empty.Empty) (*protos.GetAllProductsResponse, error) {
	_products, err := s.u.GetAllProducts()
	if err != nil {
		return nil, err
	}

	products := []*protos.Product{}
	for _, _product := range _products {

		product := &protos.Product{
			Id: _product.ID,
			Created: _product.Created.String(),
			Updated: _product.Updated.String(),
			Sku:   _product.SKU,
			Name:  _product.Name,
			Price: _product.Price,
			Qty:   _product.Qty,
		}

		// product.CreatedAt, _ = _product.Created.MarshalBinary()
		// product.UpdatedAt, _ = _product.Updated.MarshalBinary()
		// created, _ := _product.Created.MarshalBinary()

		products = append(products, product)
	}

	response := protos.GetAllProductsResponse{
		Products: products,
	}
	return &response, nil
}

// @Created 26/10/2021
// 
func (s *catalogService) UpdatePrice(ctx context.Context, request *protos.UpdatePriceRequest) (*protos.UpdatePriceResponse, error) {
	response := protos.UpdatePriceResponse{}
	product := entity.Product{
		ID: request.Id,
		SKU: request.Sku,
		Price: request.Price,
	}

	if err := s.u.UpdateProductPrice(&product); err != nil {
		return &response, err
	}

	response.Product = &protos.Product {
		Id: product.ID,
		Created: product.Created.String(),
		Updated: product.Updated.String(),
		Sku: product.SKU,
		Name: product.Name,
		Price: product.Price,
		Qty: product.Qty,
	}

	return &response, nil
}

// @Created 17/10/2021
// @Updated
func (s *catalogService) UpdateQuantity(ctx context.Context, request *protos.UpdateQuantityRequest) (*protos.UpdateQuantityResponse, error) {
	response := protos.UpdateQuantityResponse{}

	product, err := s.u.AddProductQuantity(request.Sku, request.Qty)
	if err != nil {
		return &response, err
	}

	response.Product = &protos.Product{
		Id:    product.ID,
		Sku:   request.Sku,
		Name:  product.Name,
		Price: product.Price,
		Qty:   product.Qty,
	}

	return &response, nil
}

// @Created 17/10/2021
// @Updated
func (s *catalogService) CheckoutProduct(ctx context.Context, request *protos.CheckoutProductRequest) (*protos.CheckoutProductResponse, error) {
	response := protos.CheckoutProductResponse{}

	product, err := s.u.CheckoutProduct(request.Sku, request.Qty)
	if err != nil {
		return &response, err
	}

	response.Product = &protos.Product{
		Id:    product.ID,
		Sku:   product.SKU,
		Name:  product.Name,
		Price: product.Price,
		Qty:   product.Qty,
	}
	return &response, nil
}
