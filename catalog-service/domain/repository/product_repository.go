package domain

import "catalog-service/domain/entity"

// @Created 12/10/2021
// @Updated 26/10/2021
type ProductRepository interface {
	Initialize() error
	Insert(*entity.Product) error
	GetByID(string) (*entity.Product, error)
	GetBySKU(string) (*entity.Product, error)
	GetByIDAndSKU(string, string) (*entity.Product, error)
	GetAll() ([]entity.Product, error)
	UpdateQtyBySKU(string, uint32) (*entity.Product, error)
	UpdatePrice(*entity.Product) error
	UpdateQty(*entity.Product) error
}
