package usecase

import (
	"catalog-service/domain/entity"
	domain "catalog-service/domain/repository"
	"errors"

	"github.com/jinzhu/gorm"
)

// @Created 12/10/2021
// @Updated 26/10/2021
type ProductUsecase interface {
	InsertNewProduct(*entity.Product) (*entity.Product, error)
	GetProductBySKU(string) (*entity.Product, error)
	GetProductByName(string) (*entity.Product, error)
	GetAllProducts() ([]entity.Product, error)
	UpdateProductPrice(*entity.Product) error
	UpdateProductQuantity(*entity.Product) error
	AddProductQuantity(string, uint32) (*entity.Product, error)
	CheckProduct(string) (bool, *entity.Product, error)
	CheckoutProduct(string, uint32) (*entity.Product, error)
}

// @Created 12/10/2021
// @Updated
type productUsecase struct {
	r domain.ProductRepository
}

// @Created 12/10/2021
// @Updated
func NewProductUsecase(r domain.ProductRepository) ProductUsecase {
	return &productUsecase{r}
}

// @Created 17/10/2021
// @Updated
func (u *productUsecase) InsertNewProduct(product *entity.Product) (*entity.Product, error) {
	isExist, _, err := u.CheckProduct(product.SKU)

	if !isExist {
		if err := u.r.Insert(product); err != nil {
			return nil, err
		}

		return product, err
	}

	return nil, errors.New("Product already exists")
}

// @Created 17/10/2021
// @Updated
func (u *productUsecase) GetProductBySKU(sku string) (*entity.Product, error) {
	return u.r.GetBySKU(sku)
}

// @Created 17/10/2021
// @Updated
func (u *productUsecase) GetProductByName(name string) (*entity.Product, error) {
	return nil, nil
}

// @Created 17/10/2021
// @Updated
func (u *productUsecase) GetAllProducts() ([]entity.Product, error) {
	return u.r.GetAll()
}

// @Created 26/10/2021
// @Updated
func (u *productUsecase) UpdateProductPrice(product *entity.Product) error {
	return u.r.UpdatePrice(product)
}

// @Created 26/10/2021
// @Updated
func (u *productUsecase) UpdateProductQuantity(product *entity.Product) error {
	return nil
}

// @Created 17/10/2021
// @Updated
func (u *productUsecase) AddProductQuantity(sku string, qty uint32) (*entity.Product, error) {
	totalQty := qty

	isExist, product, err := u.CheckProduct(sku)
	if err != nil {
		return nil, err
	}

	if !isExist {
		return nil, errors.New("Product not found")
	}
	totalQty += product.Qty

	return u.r.UpdateQtyBySKU(sku, totalQty)
}

// CheckProduct ...
// Check whether product is exist in database
// @Created 17/10/2021
// @Updated
func (u *productUsecase) CheckProduct(sku string) (bool, *entity.Product, error) {
	product, err := u.r.GetBySKU(sku)
	if err != nil {

		// Success query, however no record found
		// Alter error to nil, to prevent false positive condition
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return false, nil, nil
		}

		return false, nil, err
	}

	return true, product, nil
}

// @Created 17/10/2021
// @Updated
func (u *productUsecase) CheckoutProduct(sku string, qty uint32) (*entity.Product, error) {
	isExist, _product, err := u.CheckProduct(sku)
	if err != nil {
		return nil, err
	}

	if !isExist {
		return nil, errors.New("Product not found")
	}

	if _product.Qty < qty {
		return nil, errors.New("Order quantity exceeded item stock")
	}

	currentInventoryQty := _product.Qty - qty
	if _, err := u.r.UpdateQtyBySKU(sku, currentInventoryQty); err != nil {
		return nil, err
	}

	totalPrice := _product.Price * float64(qty)

	product := entity.Product{
		ID:    _product.ID,
		SKU:   _product.SKU,
		Name:  _product.Name,
		Price: totalPrice,
		Qty:   qty,
	}
	return &product, nil
}
