package entity

import "time"

// @Created 16/10/2021
// @Updated
type Product struct {
	ID      string    `json:"id,omitempty"`
	Created time.Time `json:"created,omitempty"`
	Updated time.Time `json:"updated,omitempty"`
	SKU     string    `json:"sku"`
	Name    string    `json:"name"`
	Price   float64   `json:"price"`
	Qty     uint32    `json:"qty"`
}
