package datasource

import (
	"github.com/mushoffa/go-library/database"
)

// @Created 12/10/2021
// @Updated 16/10/2021
type Database interface {
	database.Database
	InitializeDatabase() error
}
