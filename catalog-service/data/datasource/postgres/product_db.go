package postgres

import (
	// "log"
	_ "errors"
	_ "fmt"
	_ "time"

	"catalog-service/config"
	"catalog-service/data/datasource"
	"catalog-service/data/model"

	"github.com/mushoffa/go-library/database"
)

const (
	maxOpenConns    = 60
	connMaxLifetime = 120
	maxIdleConns    = 30
	connMaxIdleTime = 20
)

// @Created 12/10/2021
// @Updated 16/10/2021
type ProductDB struct {
	database.Database
}

// @Created 12/10/2021
// @Updated 16/10/2021
func NewProductDB(cfg *config.Config) (datasource.Database, error) {
	db, err := database.NewPostgres(
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresDB,
		cfg.PostgresUser,
		cfg.PostgresPassword,
		false,
	)

	productDB := &ProductDB{db}
	return productDB, err
}

func (db *ProductDB) InitializeDatabase() error {
	db.AutoMigrate(&model.Product{})
	return nil
}
