package model

import (
	"time"
)

// @Created 12/10/2021
// @Updated 13/10/2021
type Product struct {
	ID      string    `gorm:"column:id"`
	Created time.Time `gorm:"column:created"`
	Updated time.Time `gorm:"column:updated"`
	SKU     string    `gorm:"column:sku"`
	Name    string    `gorm:"column:name"`
	Price   float64   `gorm:"column:price"`
	Qty     uint32    `gorm:"column:qty"`
}

// @Created 12/10/2021
// @Updated
func (t *Product) TableName() string {
	return "products"
}
