package repository

import (
	"catalog-service/data/datasource"
	"catalog-service/data/model"
	"catalog-service/domain/entity"
	domain "catalog-service/domain/repository"
	"time"

	"github.com/google/uuid"
	// "github.com/mushoffa/go-library/database"
)

// @Created 12/10/2021
// @Updated
type productRepository struct {
	db datasource.Database
}

// @Created 12/10/2021
// @Updated
func NewProductRepository(db datasource.Database) domain.ProductRepository {
	return &productRepository{db: db}
}

func (r *productRepository) Initialize() error {
	r.db.InitializeDatabase()
	return nil
}

// @Created 16/10/2021
// @Updated
func (r *productRepository) Insert(product *entity.Product) error {

	id := uuid.New().String()
	created := time.Now()

	productDB := model.Product{
		ID:      id,
		Created: created,
		SKU:     product.SKU,
		Name:    product.Name,
		Price:   product.Price,
		Qty:     product.Qty,
	}

	if err := r.db.Create(&productDB); err != nil {
		return err
	}

	product.ID = id
	product.Created = created

	return nil
}

// @Created 26/10/2021
// @Updated 
func (r *productRepository) GetByID(id string) (*entity.Product, error) {
	product := entity.Product{}

	if err := r.db.FindByID("id", id, &product); err != nil {
		return nil, err
	}

	return &product, nil
}

// @Created 13/10/2021
// @Updated 16/10/2021
func (r *productRepository) GetBySKU(sku string) (*entity.Product, error) {
	product := entity.Product{}

	if err := r.db.FindByID("sku", sku, &product); err != nil {
		return nil, err
	}

	return &product, nil
}

// @Created 26/10/2021
// @Updated 
func (r *productRepository) GetByIDAndSKU(id, sku string) (*entity.Product, error) {
	product := entity.Product{}

	if err := r.db.GetInstance().Model(&product).
		Where("id = ?", id).
		Where("sku = ?", sku).
		Take(&product).Error; err != nil {
			return nil, err
		}

	return &product, nil
}

// @Created 13/10/2021
// @Updated 16/10/2021
func (r *productRepository) GetAll() ([]entity.Product, error) {
	products := []entity.Product{}

	if err := r.db.FindAll(&products); err != nil {
		return nil, err
	}

	return products, nil
}

// @Created 16/10/2021
// @Updated
func (r *productRepository) UpdateQtyBySKU(sku string, qty uint32) (*entity.Product, error) {
	product := entity.Product{}

	if err := r.db.UpdateByID("sku", sku, "qty", qty, &product); err != nil {
		return nil, err
	}
	return &product, nil
}

// @Created 26/10/2021
// @Updated
func (r *productRepository) UpdatePrice(product *entity.Product) (error) {

	if _,err := r.GetByIDAndSKU(product.ID, product.SKU); err != nil {	
		return err
	}
	
	product.Updated = time.Now()

	if err := r.db.GetInstance().Model(&model.Product{}).
		Where("id = ?", product.ID).
		Where("sku = ?", product.SKU).
		Update("price", product.Price).
		Update("updated", product.Updated).Error; err != nil {
			return err
		}

	return nil
}

// @Created 26/10/2021
// @Updated
func (r *productRepository) UpdateQty(product *entity.Product) (error) {

	if _,err := r.GetByIDAndSKU(product.ID, product.SKU); err != nil {
		return err
	}
	
	product.Updated = time.Now()

	if err := r.db.GetInstance().Model(&model.Product{}).
		Where("id = ?", product.ID).
		Where("sku = ?", product.SKU).
		Update("qty", product.Qty).
		Update("updated", product.Updated).Error; err != nil {
			return err
		}

	return nil
}