module catalog-service

go 1.17

require (
	github.com/golang/protobuf v1.5.2
	github.com/kelseyhightower/envconfig v1.4.0
	google.golang.org/grpc v1.41.0
	google.golang.org/protobuf v1.27.1
)

require (
	github.com/google/uuid v1.1.2
	github.com/jinzhu/gorm v1.9.16
	github.com/mushoffa/go-library v0.0.0-20211016054407-67a21adc8585
	golang.org/x/text v0.3.6 // indirect
	google.golang.org/genproto v0.0.0-20211011165927-a5fb3255271e // indirect
)

require (
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/lib/pq v1.1.1 // indirect
	github.com/mushoffa/bcgdv-kuncie v0.0.0-20211026102353-f827cf34c273 // indirect
	golang.org/x/net v0.0.0-20210405180319-a5a99cb37ef4 // indirect
	golang.org/x/sys v0.0.0-20210510120138-977fb7262007 // indirect
)
