# BCGDV Kuncie

## Contents
1. [Introduction](#introduction)
    * [Problem Statement](#problem-statement)
2. [Overview](#overview)
    * [System Architecture](#system-architecture)
    * [Application Architecture](#application-architecture)
3. [Getting Started](#getting-started)
    * [Prerequisites](#prerequisites)

## Introduction
### Problem Statement
| SKU   | Name  | Price | Inventory Qty |
| :---: | --- | ---   |          ---: |
| 120P90 | Google Home | $49.99 | 10 |
| 43N23P | MacBook Pro | $5,399.99 | 5 |
| A304SD | Alexa Speaker | $109.50 | 10 |
| 234234 | Raspberry Pi B | $30.00 | 2 |

**The system should have the following promotions:**
- Each sale of a MacBook Pro comes with a free Raspberry Pi B.
- Buy 3 Google Home for the price of 2.
- Buying more than 3 Alexa Speakers will have 10% discount on all Alexa Speakers.

## Overview
### System Architecture
![](assets/bcgdv_kuncie_system_architecture.png)

### Application Architecture
![](assets/api_gateway_architecture.png)
![](assets/product_service_architecture.png)
![](assets/order_service_architecture.png)
![](assets/promotion_service_architecture.png)

## Getting Started
### Prerequisites
1.  Please make sure you have [Go](https://golang.org/doc/install) installed on your system.
2.  Please make sure you have [Docker](https://docs.docker.com/engine/install/) installed on your system.
3.  Please make sure you have [Docker Compose](https://docs.docker.com/compose/install/) installed on your system.
4.  Please make sure you have [gRPC Go Plugin](https://grpc.io/docs/languages/go/quickstart/) installed on your system.
5.  Please make sure you have [grpcurl](https://github.com/fullstorydev/grpcurl) installed on your system. It is a command-line tools to communicate to gRPC server.
