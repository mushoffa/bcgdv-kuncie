package server

import (
	"context"
	"log"
	"net"
	"os"
	"os/signal"
	"syscall"

	"order-service/client"
	"order-service/config"
	"order-service/data/datasource/postgres"
	"order-service/data/repository"
	"order-service/domain/entity/pb"
	"order-service/domain/usecase"
	"order-service/service"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

// @Created 14/10/2021
// @Updated
type Server struct {
	cfg *config.Config
}

// 14/10/2021
type grpcServer struct {
	// service service.OrderService
	service         pb.OrderServiceServer
	promotionClient *client.PromotionClient
}

// @Created 14/10/2021
// @Updated
func NewServer(cfg *config.Config) *Server {
	return &Server{cfg}
}

// @Created 14/10/2021
// @Updated 18/10/2021
func (server *Server) Run() error {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	errChannel := make(chan error, 1)
	signalChannel := make(chan os.Signal, 1)

	db, err := postgres.NewOrderDB(server.cfg)
	if err != nil {
		errChannel <- err
	}

	promotionServer, err := client.NewPromotionClient(":9093")
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
		errChannel <- err
	}

	productServer, err := client.NewProductClient(":9091")
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
		errChannel <- err
	}

	listen, err := net.Listen("tcp", server.cfg.ServerPort)
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
		errChannel <- err
	}

	promotionClient := pb.NewPromotionServiceClient(promotionServer.Conn)
	productClient := pb.NewCatalogServiceClient(productServer.Conn)
	rc := repository.NewCartRepository(db)
	ri := repository.NewItemRepository(db)
	rc.Initialize()
	ri.Initialize()
	// r.GetAllProducts()
	u := usecase.NewOrderUsecase(promotionClient, productClient, rc, ri)
	s := service.NewOrderService(u)

	srv := grpc.NewServer()
	pb.RegisterOrderServiceServer(srv, s)
	reflection.Register(srv)

	// log.Printf("server listening at %v", listen.Addr())
	// if err := srv.Serve(listen); err != nil {
	// 	log.Fatalf("failed to serve: %v", err)
	// }

	go func() {
		log.Printf("Server listening on port: %v", listen.Addr())
		// log.Println("Server is listening on port: ", server.cfg.Server.Port)
		errChannel <- srv.Serve(listen)
	}()

	signal.Notify(signalChannel, syscall.SIGINT, syscall.SIGTERM)

	select {
	case err := <-errChannel:
		if err != nil {
			log.Fatalf("Error: %v", err)
		}

	case quit := <-signalChannel:
		log.Fatalf("signal.Notify: %v", quit)

	case done := <-ctx.Done():
		log.Fatalf("ctx.Done(): %v", done)
	}

	// server.GracefulStop()
	// if err := srv.Shutdown(ctx); err != nil {
	// 	log.Fatalf("Server forced to shutdown: %v", err)
	// }

	return nil

	// return srv.Serve(listen)
}
