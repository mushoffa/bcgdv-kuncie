package repository

import (
	// "log"
	"time"

	"order-service/data/datasource"
	"order-service/data/model"
	"order-service/domain/entity"
	domain "order-service/domain/repository"

	_ "github.com/google/uuid"
)

// @Created 14/10/2021
// @Updated
type itemRepository struct {
	db datasource.Database
}

// @Created 14/10/2021
// @Updated
func NewItemRepository(db datasource.Database) domain.ItemRepository {
	return &itemRepository{db}
}

// @Created 20/10/2021
// @Updated
func (r *itemRepository) Initialize() error {
	r.db.GetInstance().AutoMigrate(&model.Item{})
	return nil
}

// @Created 14/10/2021
// @Updated
func (r *itemRepository) AddItem(cartId, sku, name string, price float64, qty uint32) error {

	item := model.Item{
		CartID: cartId,
		Created: time.Now(),
		SKU: sku,
		Name: name,
		Price: price,
		Qty: qty,
	}
	
	if err := r.db.Create(&item); err != nil {
		return err
	}

	return nil
}

// @Created 14/10/2021
// @Updated
func (r *itemRepository) CheckExistingItem(cartId, sku string) (string, uint32, error) {
	item := model.Item{}

	// if err := r.db.FindByID("sku", sku, &item); err != nil {
	// 	return "", 0, err
	// }
	err := r.db.GetInstance().Model(&item).
		Where("cart_id = ? ", cartId).
		Where("sku = ?", sku).
		Take(&item).Error
	if err != nil {
		return "", 0, err
	}

	return item.SKU, item.Qty, nil
}

// @Created 14/10/2021
// @Updated
func (r *itemRepository) UpdateItemQuantity(cartId, sku string, qty uint32) error {
	item := model.Item{}

	err := r.db.GetInstance().Model(&item).
		Where("cart_id = ?", cartId).
		Where("sku = ?", sku).
		Update("qty", qty).
		Update("updated", time.Now()).Error
	if err != nil {
		return err
	}
	// if err := r.db.UpdateByID("sku", sku, "qty", qty, &item); err != nil {
	// 	return err
	// }

	return nil
}

// @Created 14/10/2021
// @Updated
func (r *itemRepository) GetAllItems(id string) ([]entity.Item, error) {
	// items := []entity.OrderItem{}
	items := []entity.Item{}
	// itemsDB := []model.Item{}

	// res, err := r.db.FindAll(&items)
	// if err != nil {
	// 	return nil, err
	// }
	r.db.GetInstance().Where("cart_id = ?", id).Find(&items)

	// for _, item := range itemsDB {
	// 	items = append(items, entity.Item{
	// 		SKU: item.SKU,
	// 		Name: item.Name,
	// 		Price: item.Price,
	// 		Qty: item.Qty,
	// 	},)
	// }
	return items, nil
}