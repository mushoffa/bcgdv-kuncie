package repository

import (
	// "log"
	"time"

	"order-service/data/datasource"
	"order-service/data/model"
	domain "order-service/domain/repository"
	"order-service/domain/entity"

	"github.com/google/uuid"
)

// @Created 14/10/2021
// @Updated
type cartRepository struct {
	db datasource.Database
}

// @Created 14/10/2021
// @Updated
func NewCartRepository(db datasource.Database) domain.CartRepository {
	return &cartRepository{db}
}

// @Created 20/10/2021
// @Updated
func (r *cartRepository) Initialize() error {
	r.db.GetInstance().AutoMigrate(&model.Cart{})
	return nil
}

// @Created 14/10/2021
// @Updated 20/10/2021
func (r *cartRepository) CreateCart(cart *entity.Cart) (error) {
	id := uuid.NewString()
	created := time.Now()
	totalPrice := float64(0)

	cartDB := model.Cart{
		ID: id,
		Created: created,
		Name: cart.Name,
		TotalPrice: totalPrice,
	}

	err := r.db.Create(&cartDB)
	if err != nil {
		return err
	}

	cart.ID = id
	cart.Created = created
	cart.TotalPrice = totalPrice

	return nil
}

// @Created 14/10/2021
// @Updated
func (r *cartRepository) CheckCartID(id string) (float64,error) {
	cart := model.Cart{}

	if err := r.db.FindByID("id", id, &cart); err != nil {
		return 0, err
	}

	return cart.TotalPrice, nil
}

// @Created 20/10/2021
// @Updated
func (r *cartRepository) GetCartByID(id string) (*entity.Cart, error) {
	cart := entity.Cart{}
	// items := []entity.OrderItem{}

	if err := r.db.FindByID("id", id, &cart); err != nil {
		return nil, err
	}	

	// if err := r.db.FindByID("cart_id", id, &items); err != nil {
	// 	return nil, err
	// }

	// cart.OrderItems = items 

	return &cart, nil
}

// @Created 20/10/2021
// @Updated
func (r *cartRepository) GetAllCarts() ([]entity.Cart, error) {
	carts := []entity.Cart{}

	if err := r.db.FindAll(&carts); err != nil {
		return nil, err
	}

	return carts, nil
}

// @Created 14/10/2021
// @Updated
func (r *cartRepository) UpdateCartTotalPrice(id string, price float64) error {
	cart := model.Cart{}

	if err := r.db.UpdateByID("id", id, "total_price", price, &cart); err != nil {
		return err
	}


	return nil
}