package datasource

import (
	"github.com/jinzhu/gorm"
	"github.com/mushoffa/go-library/database"
)

// @Created 13/10/2021
// @Updated 20/10/2021
type Database interface {
	database.Database
	InitializeDatabase() error
}

// @Created 13/10/2021
// @Updated 14/10/2021
type Reader interface {
	FindAll(interface{}) (interface{},error)
	GetInstance() *gorm.DB
	FindByID(string, string, interface{}) (error)
}

// @Created 13/10/2021
// @Updated 14/10/2021
type Writer interface {
	//AutoMigrate(...interface{})
	Create(interface{}) error
	UpdateByID(string, string, string, interface{}, interface{}) (error)
}