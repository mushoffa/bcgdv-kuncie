package postgres

import (
	"order-service/config"
	"order-service/data/datasource"
	"order-service/data/model"

	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/mushoffa/go-library/database"
)

const (
	maxOpenConns    = 60
	connMaxLifetime = 120
	maxIdleConns    = 30
	connMaxIdleTime = 20
)

// @Created 13/10/2021
// @Updated 20/10/2021
type OrderDB struct {
	database.Database
}

// @Created 13/10/2021
// @Updated
func NewOrderDB(cfg *config.Config) (datasource.Database, error) {
	db, err := database.NewPostgres(
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresDB,
		cfg.PostgresUser,
		cfg.PostgresPassword,
		false,
	)

	orderDB := &OrderDB{db}
	return orderDB, err
}

func (db *OrderDB) InitializeDatabase() error {
	db.AutoMigrate(&model.Cart{})
	db.AutoMigrate(&model.Item{})
	return nil
}