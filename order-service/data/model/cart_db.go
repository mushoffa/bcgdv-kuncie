package model

import (
	"time"
)

// @Created 13/10/2021
// @Updated 14/10/2021
type Cart struct {
	ID 			string 		`gorm:"column:id"`
	Created 	time.Time 	`gorm:"column:created"`
	Updated 	time.Time 	`gorm:"column:updated"`
	Name 		string 		`gorm:"column:name"`
	TotalPrice 	float64 	`gorm:"column:total_price"`
}

// @Created 13/10/2021
// @Updated 
func (t *Cart) TableName() string {
	return "carts"
}