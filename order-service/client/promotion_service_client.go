package client

import (
	_ "context"
	"log"
	_ "time"

	"order-service/domain/entity/pb"

	"google.golang.org/grpc"
)

// @Created 14/10/2021
// @Updated 
type PromotionClient struct {
	Conn *grpc.ClientConn
	Service pb.PromotionServiceClient
}

// @Created 14/10/2021
// @Updated 
func NewPromotionClient(url string) (*PromotionClient, error) {
	conn, err := grpc.Dial(url, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Could not connect to Pomotion Service Client: %s", err)
		return nil, err
	}
	// defer conn.Close()

	c := pb.NewPromotionServiceClient(conn)
	return &PromotionClient{conn, c}, nil

	// ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	// defer cancel()
}

// @Created 14/10/2021
// @Updated 
func (c *PromotionClient) Close() {
	c.Conn.Close()
}