package client

import (
	_ "context"
	"log"
	_ "time"

	"order-service/domain/entity/pb"

	"google.golang.org/grpc"
)

// @Created 14/10/2021
// @Updated 
type ProductClient struct {
	Conn *grpc.ClientConn
	Service pb.CatalogServiceClient
}

// @Created 14/10/2021
// @Updated 
func NewProductClient(url string) (*ProductClient, error) {
	conn, err := grpc.Dial(url, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Could not connect to Product Service Client: %s", err)
		return nil, err
	}
	// defer conn.Close()

	c := pb.NewCatalogServiceClient(conn)
	return &ProductClient{conn, c}, nil

	// ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	// defer cancel()
}

// @Created 14/10/2021
// @Updated 
func (c *ProductClient) Close() {
	c.Conn.Close()
}