package service

import (
	"context"
	// "fmt"
	"log"

	"order-service/domain/entity/pb"
	"order-service/domain/usecase"

	"github.com/golang/protobuf/ptypes/empty"
)


// @Created 14/10/2021
// @Updated
type OrderService interface {
	
}

// @Created 14/10/2021
// @Updated
type orderService struct {
	pb.UnimplementedOrderServiceServer
	u usecase.OrderUsecase
}

// @Created 14/10/2021
// @Updated
func NewOrderService(u usecase.OrderUsecase) pb.OrderServiceServer {
	return &orderService{u:u}
}

// @Created 14/10/2021
// @Updated
func (s *orderService) CreateCart(ctx context.Context, request *pb.CreateCartRequest) (*pb.CreateCartResponse, error) {
	res, err := s.u.CreateNewOrderCart(request.Name)
	if err != nil {
		return nil, err
	}

	response := pb.CreateCartResponse {
		Id: res.ID,
		Name: request.Name,
	}

	return &response, nil
}

// @Created 20/10/2021
// @Updated
func (s *orderService) GetCartByID(ctx context.Context, request *pb.GetCartByIDRequest) (*pb.GetCartByIDResponse, error) {

	cart, err := s.u.GetCartByID(request.Id)
	if err != nil {
		return nil, err
	}

	items := []*pb.OrderProduct{}
	for _, _item := range cart.OrderItems {
		item := &pb.OrderProduct{
			Sku: _item.SKU,
			Name: _item.Name,
			Price: _item.Price,
			Qty: _item.Qty,
		}

		items = append(items, item)
	}

	// response.Id = cart.ID
	// response.Name = cart.Name
	// response.TotalPrice = cart.TotalPrice

	// fmt.Println("Response: ", response)

	// response.Cart = &pb.Cart{
	// 	// Id: cart.ID,
	// 	// Name: cart.Name,
	// 	TotalPrice: cart.TotalPrice,
	// 	Products: items,
	// }

	response := pb.GetCartByIDResponse{
		Id: cart.ID,
		Name: cart.Name,
		TotalPrice: float64(cart.TotalPrice),
		Items: items,
	}

	return &response, nil
}

// @Created 20/10/2021
// @Updated
func (s *orderService) GetAllCarts(ctx context.Context, request *empty.Empty) (*pb.GetAllCartsResponse, error) {
	response := pb.GetAllCartsResponse{}

	_carts, err := s.u.GetAllCarts()
	if err != nil {
		return &response, err
	}

	carts := []*pb.Cart{}
	for _, _cart := range _carts {
		cart := &pb.Cart{
			Id: _cart.ID,
			Name: _cart.Name,
			TotalPrice: _cart.TotalPrice,
		}

		carts = append(carts, cart)
	}

	response.Carts = carts

	return &response, nil
}

// @Created 14/10/2021
// @Updated 20/10/2021
func (s *orderService) AddOrderProduct(ctx context.Context, request *pb.AddOrderProductRequest) (*pb.AddOrderProductResponse, error) {
	// log.Println(request.Product)
	// res, err := s.u.GetCartByID(request.Id)
	// if err != nil {
	// 	return nil, err
	// }

	res, err := s.u.AddOrderProduct(request.Id, request.Sku, request.Qty)
	if err != nil {
		return nil, err
	}

	log.Println("AddOrderProduct ==> ", res)

	products := []*pb.OrderProduct{}
	for _, item := range res.OrderItems {
		// p := pb.OrderProduct{}
		products = append(products, &pb.OrderProduct{
			Sku: item.SKU,
			Name: item.Name,
			Price: item.Price,
			Qty: item.Qty,
		})
	}

	response := pb.AddOrderProductResponse{
		Cart: &pb.Cart {
			Id: res.ID,
			Name: res.Name,
			TotalPrice: res.TotalPrice,
			Products: products,
		},
	}

	return &response, nil
}

// @Created 14/10/2021
// @Updated
func (s *orderService) CheckoutCart(ctx context.Context, request *pb.CheckoutCartRequest) (*pb.CheckoutCartResponse, error) {
	// log.Println(request.Product)
	res, err := s.u.CheckOrderCartID(request.Id)
	if err != nil {
		return nil, err
	}

	items, err := s.u.CheckoutCart(request.Id)
	if err != nil {
		return nil, err
	}

	products := []*pb.OrderProduct{}
	for _, item := range items {
		// p := pb.OrderProduct{}
		products = append(products, &pb.OrderProduct{
			Sku: item.SKU,
			Name: item.Name,
			Price: item.Price,
			Qty: item.Qty,
		})
	}

	response := pb.CheckoutCartResponse{
		Order: &pb.Cart {
			Id: request.Id,
			TotalPrice: res,
			Products: products,
		},
	}

	return &response, nil
}