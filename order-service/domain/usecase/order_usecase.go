package usecase

import (
	"context"
	"log"

	_ "order-service/client"
	"order-service/domain/entity"
	"order-service/domain/entity/pb"
	domain "order-service/domain/repository"
)

// @Created 14/10/2021
// @Updated 20/10/2021
type OrderUsecase interface {
	CreateNewOrderCart(string) (*entity.Cart, error)
	CheckOrderCartID(string) (float64, error)
	CheckExistingOrderProduct(string, string) (bool, uint32)
	GetCartByID(string) (*entity.Cart, error)
	GetAllCarts() ([]entity.Cart, error)
	AddOrderProduct(string, string, uint32) (*entity.Cart, error)
	CheckoutCart(string) ([]entity.Item, error)
}

// @Created 14/10/2021
// @Updated 20/10/2021
type orderUsecase struct {
	promotionClient pb.PromotionServiceClient
	productClient pb.CatalogServiceClient
	rc              domain.CartRepository
	ri              domain.ItemRepository
}

// @Created 14/10/2021
// @Updated
func NewOrderUsecase(promotionClient pb.PromotionServiceClient, productClient pb.CatalogServiceClient, rc domain.CartRepository, ri domain.ItemRepository) OrderUsecase {
	return &orderUsecase{
		promotionClient: promotionClient, 
		productClient: productClient, 
		rc:rc, 
		ri:ri,
	}
}

// @Created 14/10/2021
// @Updated
func (u *orderUsecase) CreateNewOrderCart(name string) (*entity.Cart, error) {
	cart := &entity.Cart{Name: name}

	if err := cart.ValidateName(); err != nil {
		return nil, err
	}

	if err := u.rc.CreateCart(cart); err != nil {
		return nil, err
	}

	return cart, nil
}

// @Created 20/10/2021
// @Updated
func (u *orderUsecase) GetCartByID(id string) (*entity.Cart, error) {
	cart, err := u.rc.GetCartByID(id)
	if err != nil {
		return nil, err
	}

	items, err := u.ri.GetAllItems(id)
	if err != nil {
		return nil , err
	}

	log.Println("Items: ", items)
	cart.OrderItems = items

	return cart, nil
}

// @Created 20/10/2021
// @Updated
func (u *orderUsecase) GetAllCarts() ([]entity.Cart, error) {
	return u.rc.GetAllCarts()
}

// @Created 14/10/2021
// @Updated
func (u *orderUsecase) CheckOrderCartID(id string) (float64, error) {
	return u.rc.CheckCartID(id)
}

// @Created 14/10/2021
// @Updated
func (u *orderUsecase) AddOrderProduct(cartId, sku string, qty uint32) (*entity.Cart, error) {
	cart, err := u.GetCartByID(cartId)
	if err != nil {
		return nil, err
	}

	response, err := u.productClient.GetProductBySKU(context.Background(), &pb.GetProductBySKURequest{Sku:sku})
	if err != nil {
		return nil, err
	}

	currentTotalPrice := cart.TotalPrice
	totalQuantity := qty

	isItemExist, _qty := u.CheckExistingOrderProduct(cartId, sku)

	// _, err = u.promotionClient.CheckPromotion(context.Background(), &pb.CheckPromotionRequest{
	// 	Sku: sku,
	// 	Qty: qty,
	// })
	// if err != nil {
	// 	log.Println("ErrorCheckPromotion: ", err)
	// }

	if !isItemExist {
		if err := u.ri.AddItem(cartId, sku, response.Product.Name, response.Product.Price, qty); err != nil {
			return nil, err
		}
	} else {
		// if err := u.ri.UpdateItemQuantity()
		totalQuantity += _qty
		if err := u.ri.UpdateItemQuantity(cartId, sku, totalQuantity); err != nil {
			return nil, err
		}
	}

	totalPrice := currentTotalPrice + (response.Product.Price * float64(qty))

	if err := u.rc.UpdateCartTotalPrice(cartId, totalPrice); err != nil {
		return nil, err
	}

	// cart.OrderItems = append(cart.OrderItems, entity.Item{
	// 	SKU: sku,
	// 	Name: response.Product.Name,
	// 	Price: response.Product.Price,
	// 	Qty: qty,
	// })
	cart.TotalPrice = totalPrice
	return cart, nil
}

// @Created 14/10/2021
// @Updated
func (u *orderUsecase) CheckExistingOrderProduct(cartId, sku string) (bool, uint32) {
	_, qty, err := u.ri.CheckExistingItem(cartId, sku)
	if err != nil {
		// Item does not exist in order cart
		return false, 0
	}

	return true, qty
}

// @Created 14/10/2021
// @Updated
func (u *orderUsecase) CheckoutCart(id string) ([]entity.Item, error) {
	res, err := u.ri.GetAllItems(id)
	if err != nil {
		return nil, err
	}

	log.Println("CheckoutCart -> Items: ", res)
	return res, nil
}
