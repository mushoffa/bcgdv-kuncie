package entity

// @Created 14/10/2021
type Item struct {
	SKU   string 	`json:"sku"`
	Name  string    `json:"name"`
	Price float64 	`json:"price"`
	Qty   uint32 	`json:"qty"`
}

// @Created 14/10/2021
// @Updated 20/10/2021
type OrderItem struct {
	SKU   string 	`json:"sku"`
	Name  string    `json:"name"`
	Price float64 	`json:"price"`
	Qty   uint32 	`json:"qty"`
}