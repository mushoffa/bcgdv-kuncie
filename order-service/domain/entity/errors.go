package entity

import (
	"errors"
)

var (
	ErrEmptyCartName = errors.New("Cart name must not be empty")
	ErrInvalidCartNameLength = errors.New("Cart name minimum is 5 characters")
)