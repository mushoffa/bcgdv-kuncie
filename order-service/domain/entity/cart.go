package entity

import (
	"time"
)

// @Created 20/10/2021
// @Updated
type Cart struct {
	ID 			string 			`json:"id"`
	Created 	time.Time 		`json:"created"`
	Updated 	time.Time 		`json:"updated"`
	Name 		string 			`json:"name"`
	TotalPrice 	float64 		`json:"totalPrice"`
	OrderItems 	[]Item      	`json:"items"`
}

// @Created 20/10/2021
// @Updated
func (e *Cart) ValidateName() error {

	if e.Name == "" {
		return ErrEmptyCartName
	}

	if len(e.Name) < 5 {
		return ErrInvalidCartNameLength
	}

	return nil
}