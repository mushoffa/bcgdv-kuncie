package domain

import (
	"order-service/domain/entity"
)

// @Created 14/10/2021
// @Updated 20/10/2021
type CartRepository interface {
	Initialize() error
	CreateCart(*entity.Cart) (error)
	CheckCartID(string) (float64, error)
	GetCartByID(string) (*entity.Cart, error)
	GetAllCarts() ([]entity.Cart, error)
	UpdateCartTotalPrice(string, float64) error
}