package domain

import (
	"order-service/domain/entity"
)

// @Created 14/10/2021
// @Updated 20/10/2021
type ItemRepository interface {
	Initialize() error
	AddItem(string, string, string, float64, uint32) error
	CheckExistingItem(string, string) (string, uint32, error)
	GetAllItems(string) ([]entity.Item, error)
	UpdateItemQuantity(string, string, uint32) error
}