package config

// @Created 13/10/2021
// @Updated 18/10/2021
type Config struct {
	ServerPort       string `envconfig:"SERVER_PORT"`
	PostgresHost     string `envconfig:"POSTGRES_HOST"`
	PostgresPort     string `envconfig:"POSTGRES_PORT"`
	PostgresDB       string `envconfig:"POSTGRES_DB"`
	PostgresUser     string `envconfig:"POSTGRES_USER"`
	PostgresPassword string `envconfig:"POSTGRES_PASSWORD"`
	PostgresSSLMode  bool   `envconfig:"POSTGRES_SSL_MODE"`
}
